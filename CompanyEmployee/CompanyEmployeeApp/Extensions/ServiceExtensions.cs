﻿using Contracts;
using LoggerService.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Service.Contracts;
using Services;

namespace CompanyEmployeeApp.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Configure system CORS policy
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureCORS(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CORSPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();

                    //builder.WithOrigins("www.test.com")
                    //    .WithMethods("GET", "POST")
                    //    .WithHeaders("accept", "content-type");
                });
            });
        }

        /// <summary>
        /// Configure IIS setting for hosting purpose
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureIISOptions(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>       // leaving the options empty to use default configurations
            {

            });
        }

        /// <summary>
        /// Configure the logger service
        /// </summary>
        /// <param name="service"></param>
        public static void ConfigureLoggingService(this IServiceCollection service)
        {
            service.AddSingleton<ILoggerManager, LoggerManager>();
        }

        /// <summary>
        /// Register the repository manger service
        /// </summary>
        /// <param name="service"></param>
        public static void ConfigureRepositoryManager(this IServiceCollection service)
        {
            service.AddScoped<IRepositoryManager, RepositoryManager>();
        }

        /// <summary>
        /// Register the service manager
        /// </summary>
        /// <param name="service"></param>
        public static void ConfigureServiceManager(this IServiceCollection service)
        {
            service.AddScoped<IServiceManager, ServiceManager>();
        }

        /// <summary>
        /// Registering the application db context
        /// </summary>
        /// <param name="service"></param>
        /// <param name="configuration"></param>
        public static void ConfigureSqlContext(this IServiceCollection service, IConfiguration configuration) =>
            service.AddDbContext<RepositoryDbContext>(options => configuration.GetConnectionString("sqlConnection"));
    }
}
