﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompanyEmployeeApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        [HttpGet("getTestData")]
        public List<string> Get()
        {
            return new List<string>()
            {
                "One", "Two", "Three"
            };
        }
    }
}
