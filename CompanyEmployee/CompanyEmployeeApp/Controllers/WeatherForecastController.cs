using System;
using System.Collections.Generic;
using System.Linq;
using LoggerService.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CompanyEmployeeApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILoggerManager _logger;

        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };


        public WeatherForecastController(ILoggerManager logger)
        {
            _logger = logger;
        }

        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogInfo("This is log info...");
            _logger.LogWarn("This is log warn...");
            _logger.LogDebug("This is log debug...");
            _logger.LogError("This is log error...");

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}