using System;
using System.IO;
using CompanyEmployeeApp.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;

var builder = WebApplication.CreateBuilder(args);

// CORS policy configure
builder.Services.ConfigureCORS();

// IIS setting configure
builder.Services.ConfigureIISOptions();

// nlog enable
LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
builder.Services.ConfigureLoggingService();

// registering services
builder.Services.ConfigureRepositoryManager();
builder.Services.ConfigureServiceManager();
builder.Services.ConfigureSqlContext(builder.Configuration);


builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseForwardedHeaders(new ForwardedHeadersOptions()
{
    ForwardedHeaders = ForwardedHeaders.All
});

app.UseCors("CORSPolicy");

app.UseAuthorization();

// custom middlewares
app.Use(async (context, next) =>
{
    Console.WriteLine("From the first middleware! Before executing...");
    await next.Invoke();
    Console.WriteLine("From the fist middleware! After the execution....");
});

// executing branch of middleware by URL
app.Map("/mapMiddleware", builder =>
{
    builder.Use(async (context, next) =>
    {
        Console.WriteLine("From map middleware! Before Executing....");
        await next.Invoke();
        Console.WriteLine("From the map middleware! After executed....");
    });
});

// (using terminate middleware: RUN())
//app.Run(async context =>
//{
//    Console.WriteLine("From the terminate middleware! Executing....");
//    await context.Response.WriteAsync("Hello fromm terminated middleware!");
//});

app.MapControllers();

app.Run();
