﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace Repository
{
    public sealed class RepositoryManager: IRepositoryManager
    {
        private readonly RepositoryDbContext _db;
        private readonly Lazy<ICompanyRepository> _company;
        private readonly Lazy<IEmployeeRepository> _employee;

        public RepositoryManager(RepositoryDbContext db)
        {
            _db = db;
            _company = new Lazy<ICompanyRepository>(() => new CompanyRepository(_db));
            _employee = new Lazy<IEmployeeRepository>(() => new EmployeeRepository(_db));
        }

        public ICompanyRepository Company => _company.Value;
        public IEmployeeRepository Employee => _employee.Value;
        public void Save() => _db.SaveChanges();
    }
}
