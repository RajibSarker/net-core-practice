﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Configurations;

namespace Repository
{
    public class RepositoryDbContext: DbContext
    {
        public RepositoryDbContext(DbContextOptions<RepositoryDbContext> options):base(options)
        {
        }

        // db sets
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Model configuration
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // apply company and employee seed data
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
        }
    }
}
