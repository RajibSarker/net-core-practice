﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Repository.Base;

namespace Repository
{
    public class CompanyRepository: BaseRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(RepositoryDbContext db) : base(db)
        {
        }
    }
}
