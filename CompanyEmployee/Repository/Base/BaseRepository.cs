﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Contracts.Base;
using Microsoft.EntityFrameworkCore;

namespace Repository.Base
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly RepositoryDbContext _db;

        public BaseRepository(RepositoryDbContext db) => _db = db;

        public IQueryable<T> FindAll(bool trackChange) =>
            !trackChange ? _db.Set<T>().AsNoTracking() : _db.Set<T>();


        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool trackChange) =>
            !trackChange ? _db.Set<T>().Where(expression).AsNoTracking() : _db.Set<T>().Where(expression);

        public void Create(T entity) => _db.Set<T>().Add(entity);

        public void Update(T entity) => _db.Set<T>().Update(entity);

        public void Delete(T entity) => _db.Set<T>().Remove(entity);
    }
}
