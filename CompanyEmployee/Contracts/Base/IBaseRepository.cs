﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Base
{
    public interface IBaseRepository<T>
    {
        IQueryable<T> FindAll(bool trackChange);
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool trackChange);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
