﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.Base;
using Entities.Models;

namespace Contracts
{
    public interface IEmployeeRepository: IBaseRepository<Employee>
    {
    }
}
